Feature: Payment
  Scenario: Successful Payment
    Given a customer with id "omar"
    And a merchant with id "mid1"
    When the merchant initiates a payment for 10 kr by the customer
    Then the payment is successful