package org.acme;

import java.util.ArrayList;
import java.util.List;
//hello world
public class PaymentService {

    public static PaymentService Instance = new PaymentService();
    List<Payment> paymentList;
    List<String> knownMerchants;
    List<String> knownCustomers;
    public PaymentService() {
        this.paymentList = new ArrayList<>();
        this.knownCustomers = new ArrayList<>();
        this.knownCustomers.add("cid1");
        this.knownMerchants = new ArrayList<>();
        this.knownMerchants.add("mid1");
    }




    public int registerPayment(Payment pt) throws Exception {
        if(!knownCustomers.contains(pt.getCid()))
        {
            throw new Exception("customer with id " +pt.getCid()+" is unknown");
        }
        if(!knownMerchants.contains(pt.getMid()))
        {
            throw new Exception("merchant with id " +pt.getMid()+" is unknown");
        }
        this.paymentList.add(pt);
       return pt.getPaymentId();

    }

    public List<Payment> listPayments()
    {
        return paymentList;
    }


    public List<Payment> getPayment(int amount, String cid, String mid)
    {
        List<Payment> result = new ArrayList<>();
        for(Payment payment : paymentList)
        {
            if(payment.getAmount()==amount && payment.getCid().equals(cid) && payment.getMid().equals(mid))
            {
                result.add(payment);
            }
        }

        return result;
    }
}
